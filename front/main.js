
// Function to create elements and set classes
function createElement(tagName, classNames) {
    const element = document.createElement(tagName);
    if (classNames) {
      classNames.forEach((className) => {
        element.classList.add(className);
      });
    }
  
    return element;
  }
  
  // Function for creating a product card based on product data
  function createProductCard(product) {
    // Creating elements to display product information
    const productCard = createElement("div", ["product-card"]);
    productCard.dataset.id = product.id;
    const productImage = createElement("img", ["product-image"]);
    const productWrapperName = createElement("div", ["product-wrapper-name"]);
    const productName = createElement("h4", ["product-name"]);
    const productWrapperPrice = createElement("div", ["product-wrapper-price"]);
    const productPrice = createElement("p", ["product-price"]);
    const productWrapperButton = createElement("div", ["product-wrapper-button"]);
    const productButton = createElement("button", ["product-button"]);
  
    // Set the content of the elements based on the received data
    productImage.src = product.images;
    productName.textContent = product.title;
    productPrice.textContent = `$${product.price}`;
    productButton.textContent = "DELETE";
  
    // Adding the created elements to the product card
    productWrapperPrice.appendChild(productPrice);
    productWrapperButton.appendChild(productButton);
    productWrapperName.appendChild(productName);
    productCard.appendChild(productImage);
    productCard.appendChild(productWrapperName);
    productCard.appendChild(productWrapperPrice);
    productCard.appendChild(productWrapperButton);
  
    // Adding an event listener to the button
    productButton.addEventListener("click", function () {
      
      fetch(`http://localhost:3000/api/products/${product.id}`, {
        method: "DELETE",
      })
        .then((response) => {
          location.reload();
        })
        .catch((error) => console.error(error));
    });
  
    return productCard;
  }
  
  // Adding an event listener to the product card
  const container = document.querySelector(".container");
  container.addEventListener("click", function (event) {
    let target = event.target;
    while (target !== container && !target.classList.contains("product-button")) {
      if (target.classList.contains("product-card")) {
        displaySidebar(target.dataset.id);
        const sidebar = document.querySelector(".sidebar-wrapper");
        sidebar.style.display = "block";
        break;
      }
      target = target.parentNode;
    }
  });
  
  // Function to create a sidebar
  function createSidebar(product) {
    //Creating elements to display product information
    const productSidebar = createElement("div", ["sidebar"]);
    const productSidebarImage = createElement("img", ["sidebar-image"]);
    const productSidebarName = createElement("h4", ["sidebar-name"]);
    const productSidebarPrice = createElement("p", ["sidebar-price"]);
    const productSidebarDescription = createElement("p", ["sidebar-description"]);
    const productSidebarCategory = createElement("p", ["sidebar-category"]);
  
    // Set the content of the elements
    productSidebarImage.src = product.images;
    productSidebarName.textContent = `${product.title}`;
    productSidebarPrice.textContent = `price: $${product.price}`;
    productSidebarDescription.textContent = `description: ${product.description}`;
    productSidebarCategory.textContent = `category: ${product.category.name}`;
  
    // Adding the created elements to the sidebar
    productSidebar.appendChild(productSidebarImage);
    productSidebar.appendChild(productSidebarName);
    productSidebar.appendChild(productSidebarCategory);
    productSidebar.appendChild(productSidebarPrice);
    productSidebar.appendChild(productSidebarDescription);
  
    return productSidebar;
  }
  
  // Function to get product data and create a sidebar
  function displaySidebar(id) {
    // Select the element on the page where the sidebar will be displayed
    const sidebarContainer = document.querySelector(".sidebar-container");
    // Getting data from the API and creating a sidebar
    fetch(`http://localhost:3000/api/products/${id}`)
      .then((response) => response.json())
      .then((data) => {
        const product = data.data;
        const sidebar = createSidebar(product);
  
        // Clearing the container before adding a new sidebar
        sidebarContainer.innerHTML = "";
  
        // Adding a sidebar to the container on the page
        sidebarContainer.appendChild(sidebar);
      })
      .catch((error) => console.error(error));
  }
  
  // Function for obtaining product data and creating product cards
  function displayProducts() {
    // Select the element on the page where the product cards will be displayed
    const productsContainer = document.querySelector(".products-container");
  
    // We receive data from the API and create product cards
    fetch("http://localhost:3000/api/products")
      .then((response) => response.json())
      .then((data) => {
        const products = data.data;
        // Create product cards for each item in the product array
        products.forEach((product) => {
          const productCard = createProductCard(product);
  
          // Adding a product card to the container on the page
          productsContainer.appendChild(productCard);
        });
      })
      .catch((error) => console.error(error));
  }
  
  displayProducts();